#include <FatReader.h>
#include <SdReader.h>
#include <avr/pgmspace.h>
#include "WaveUtil.h"
#include "WaveHC.h"

SdReader card;    // This object holds the information for the card
FatVolume fatVolume;    // This holds the information for the partition on the card
FatReader root;   // This holds the information for the filesystem on the card
FatReader file;      // This holds the information for the file we're play

WaveHC wave;      // This is the only wave (audio) object, since we will only play one at a time

byte boxOpenSensor = 14;
bool previousBoxState = 0;

long lastDebounceTime = 0;
long debounceDelay = 50;

char* fartFileNames[] = {
    "fart1.wav",
    "fart2.wav",
    "fart3.wav",
    "fart4.wav"
};

void setup() {
    Serial.begin(9600);
    pinMode(boxOpenSensor, INPUT);

    initWaveShield();
}

void loop() {
    bool currentBoxState = getBoxState();

    if (currentBoxState != previousBoxState) {
        if (currentBoxState == 1) {
            playRandomFartSound();
        } else {
            wave.stop();    
        }
    }

    previousBoxState = currentBoxState;
}

byte getBoxState() {
    byte state = previousBoxState; 

    if (debounceTimerElapsed()) {
        lastDebounceTime = millis();
        state = isBoxOpen();
    }

    return state;
}

bool isBoxOpen() {
    return analogRead(boxOpenSensor) < 850;
}

bool debounceTimerElapsed() {
    return millis() - lastDebounceTime > debounceDelay;
}

void playRandomFartSound() {
    byte randomIndex = random(0,4);
    playfile(fartFileNames[randomIndex]);
}

void initWaveShield() {
    // Set the output pins for the DAC control. This pins are defined in the library
    pinMode(2, OUTPUT);
    pinMode(3, OUTPUT);
    pinMode(4, OUTPUT);
    pinMode(5, OUTPUT);

    if (!card.init()) {
        putstring_nl("Card init. failed!");
        sdErrorCheck();
        while(1);
    }

    // enable optimize read - some cards may timeout. Disable if you're having problems
    card.partialBlockRead(true);

    // Now we will look for a FAT partition!
    uint8_t part;
    for (part = 0; part < 5; part++) {     // we have up to 5 slots to look in
        if (fatVolume.init(card, part)) {
            break;
        }
    }

    if (part == 5) {                       // if we ended up not finding one  :(
        putstring_nl("No valid FAT partition!");
        sdErrorCheck();      // Something went wrong, lets print out why
        while(1);                            // then 'halt' - do nothing!
    }

    // Lets tell the user about what we found
    putstring("Using partition ");
    Serial.print(part, DEC);
    putstring(", type is FAT");
    Serial.println(fatVolume.fatType(),DEC);     // FAT16 or FAT32?

    // Try to open the root directory
    if (!root.openRoot(fatVolume)) {
        putstring_nl("Can't open root dir!"); // Something went wrong,
        while(1);                             // then 'halt' - do nothing!
    }

    // Whew! We got past the tough parts.
    putstring_nl("Ready!");
}

void playfile(char *name) {
    // see if the wave object is currently doing something
    if (wave.isplaying) {// already playing something, so stop it!
        wave.stop(); // stop it
    }
    // look in the root directory and open the file
    if (!file.open(root, name)) {
        putstring("Couldn't open file ");
        Serial.print(name); return;
    }
    // OK read the file and turn it into a wave object
    if (!wave.create(file)) {
        putstring_nl("Not a valid WAV");
        return;
    }

    // ok time to play! start playback
    wave.play();
}

void sdErrorCheck(void)
{
    if (!card.errorCode()) return;
    putstring("\n\rSD I/O error: ");
    Serial.print(card.errorCode(), HEX);
    putstring(", ");
    Serial.println(card.errorData(), HEX);
    while(1);
}
