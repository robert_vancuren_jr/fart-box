# Fart Box #

This is a box that makes fart noises when you open it.
[See it in action](https://vine.co/v/ebEFOZY22gl)


### Hardware ###

I just used stuff I had laying around

* [Arduino Uno](http://www.arduino.cc/en/Main/ArduinoBoardUno)
* [Adafruit Wave Shield](http://www.adafruit.com/products/94)
* [3.7W Amp](http://www.adafruit.com/product/987)
* [Speaker](http://www.adafruit.com/product/1313)
* 5 volt power supply I used a portable usb phone charger
* Photoresistor
* 10k ohm resistor


![WP_20150530_16_13_58_Pro.jpg](https://bitbucket.org/repo/KA99Bq/images/3898110560-WP_20150530_16_13_58_Pro.jpg)
![Fritzing](https://bytebucket.org/robert_vancuren_jr/fart-box/raw/d3f227da21332672bed8305be4988171618b3edc/fart-box_bb.png)